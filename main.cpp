// Nombre:
// Fecha:

#include <opencv2/highgui/highgui.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main(int argc,char ** argv)
{
    IplImage *img = cvLoadImage("imagen.jpg");

    IplImage* red = cvCreateImage(              // Crea una nueva imagen monocromática para el canal rojo
                        cvGetSize( img ),       // La imagen red tendrá el mismo tamaño que img
                        img->depth,             // La imagen red tendrá el mismo depth que img
                        1                       // La imagen red tendrá un solo canal (monocromática)
                    );                          // La imagen red está inicialmente vacía

    // Recorre la imagen renglón por renglón y asigna ceros a cada uno de los tres canales
    for( int y=0; y<img->height; y++ )
    {
            uchar* pimg = (uchar*) (img->imageData + y*img->widthStep);
            for(int x=0; x<img->width; x++ ) 
            {
                    pimg[3*x+0] = 0; // B
                    pimg[3*x+1] = 0; // G
                    pimg[3*x+2] = 0; // R
            }
    }

    cvNamedWindow("Image:",1);  // Crea ventana "Image:"
    cvShowImage("Image:",img);  // Despliega imagen en ventana "Image:"

    cvWaitKey();                // Espera a que se presione una tecla
    cvDestroyWindow("Image:");	// Cierra ventana "Image:"

    cvReleaseImage(&red);		// Libera la memoria ocupada por red
    cvReleaseImage(&img);		// Libera la memoria ocupada por img

    return 0; 
}
